Put ```.clang-format``` file in core of your sources.

Use ```clang-format -style=file -i sample_style.cpp``` from command line.

Or setup IDE to use *clang-format* with style from *file*.
