/**
 * @brief Some fake code not for compiling, only to demonstrate AUTOMATIC formatting of code style.
 * Feel free to add more samples to check formatting styles.
 *
 * For more options see https://clang.llvm.org/docs/ClangFormatStyleOptions.html
 *
 * @file sample_style.cpp
 * @author Dmitriy Gertsog
 * @date 2018-08-14
 */
#include <iostream>
#include <vector>

namespace Space1 { namespace Space2 {

char multiline_string[] =
    "aaaa"
    "bbbb"
    "cccc";

enum MyEnum
{
	VAL_ONE = 1,                                 // comment one
	VAL_SECOND_TOO_VERY_LOOOOOONG = 1234567890,  // comment two
	VAL3                                         // comment three
};

#define MULTILINE_DEFINE(expr)  \
	do {                        \
		if(!(expr)) {           \
			LOG(ASSERT, #expr); \
			assert(expr);       \
		}                       \
	} while(0)

class Sample
{
public:
	Sample(int x)
	    : member1(x)
	    , member2()
	{
		int arr[] = {1, 2, 3};
	}

private:
	int member1;
	int member2;
};

template<typename T>
void function_with_looooooooong_name_and_alot_of_parameters(T long_param_name_1,
                                                            T long_param_name_2,
                                                            T long_param_name_3,
                                                            T long_param_name_4,
                                                            T long_param_name_5,
                                                            T long_param_name_6,
                                                            T long_param_name_7,
                                                            T long_param_name_8,
                                                            T long_param_name_9)
{
	if(long_param_name_1 == long_param_name_2
	   || (long_param_name_3 > long_param_name_4 && long_param_name_5 != long_param_name_6)
	   || (long_param_name_7 & long_param_name_8) != long_param_name_9) {
		// TODO(gertsog@arrival.com): do something
	}
}

void func(int *a)
{
	int x = veryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryLongCondition
	            ? loooooooong_function_name(MyMap[{composite, key}])
	            : SecondValueVeryVeryVeryVeryLong;
	switch(*a) {
		case 1: x = 11; break;
		case 2:
			*a *= 10;  // comment for 'A'
			x = 22;    // comment for 'X'
			break;
		case 10:
		default: return;
	}
}

int main(int argc, char *argv[])
{
	if(argc < 1)
		return -1;

	while(argc--)
		func(&argc);

	if(true) {
		std::vector<Sample> x = {{1}, {2}, {3}, {4}};

		int *v = new int[3]{1, 2, 3};

		LoooooooooooooooooooooooongType loooooooooooooooooooooooooooooooooooooongVariable =
		    someLooooooooooooooooooooooooooooooongFunction();

		bool value =
		    aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa + aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
		        == aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
		    && aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
		           > ccccccccccccccccccccccccccccccccccccccccccccccc;
	}

	std::cout << "Hello world!" << std::endl;
	return 0;
}

}}  // namespace Space1::Space2
